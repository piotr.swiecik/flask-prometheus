import prometheus_client
import sys
import time

from flask import Flask, request, Response
from prometheus_client import Counter, Histogram

CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')

REQUEST_COUNT = Counter(
    "request_count", "App request count",
    ["app_name", "method", "endpoint", "http_status"]
)

REQUEST_LATENCY = Histogram(
    "request_latency_seconds", "Request latency",
    ["app_name", "endpoint"]
)


app = Flask(__name__)


def start_timer():
    request.start_time = time.time()


def stop_timer(response):
    response_time = time.time() - request.start_time
    sys.stderr.write("Response time: %ss\n" % response_time)
    REQUEST_LATENCY.labels("test_app", request.path).observe(response_time)
    return response


def record_request_data(response):
    sys.stderr.write("Request path: %s Request method: %s Response status: %s\n" %
            (request.path, request.method, response.status_code))
    REQUEST_COUNT.labels("test_app", request.method, request.path, response.status_code).inc()
    return response


def setup_metrics(app):
    app.before_request(start_timer)
    app.after_request(record_request_data)
    app.after_request(stop_timer)


@app.route("/metrics/")
def metrics():
    """agent returns metrics from registry"""
    return Response(prometheus_client.generate_latest(), mimetype=CONTENT_TYPE_LATEST)


@app.route("/test_ok/")
def test():
    return "test"


@app.route("/test_error/")
def test_error():
    x = 1/0
    return "error"


@app.errorhandler(500)
def handle_error_500(error):
    return str(error), 500


if __name__ == "__main__":
    setup_metrics(app)
    app.run(host="0.0.0.0", port=5000)
