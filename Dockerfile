FROM python:3.11-slim-bullseye

RUN apt-get update && \
    apt-get install -y curl netcat && \
    curl -sSL https://install.python-poetry.org | python3 -

WORKDIR /usr/app

EXPOSE 5000

COPY poetry.lock pyproject.toml ./

ENV PATH="/root/.local/bin:$PATH"

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --without dev --no-root

COPY . .

CMD ["python", "/usr/app/main.py"]